# Dotty!

###Dot files and configurations for my linux setups (debian or arch based)

Includes:

 + Zsh (oh-my-zsh)

 + Awesome WM (rc.lua and theme)

 + Sublime Text 3

 + VIM (vim-airline)

 + Special Folder

 + Chmod codes (morse)

 + Touchpad & Intel Graphics config (Chromebook)

#### Feel Free to use it and post any issues you find!
