# ----------------------------------------------------
# Functions
#   - Music
# ----------------------------------------------------

# Music

# Print the bitrate of a song
bitrate () {
    echo `basename "$1"`: `file "$1" | sed 's/.*, \(.*\)kbps.*/\1/' | tr -d " " ` kbps
}
