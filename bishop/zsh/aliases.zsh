# ----------------------------------------------------
# Aliases
#   - MIPS
#   - Network
#   - NPM
#   - Pacman
#   - Pip
#   - Systemd
# ----------------------------------------------------

# MIPS
alias spimf='spim -file' # compile MIPS files

# Network
alias nyan='nc -v nyancat.dakko.us 23' # Nyan Cat!
alias sw='telnet towel.blinkenlights.nl' # Star Wars Episode IV, in ascii
alias net='netctl-auto list | grep "*"'
alias neswh="sudo netctl-auto switch-to"

# NPM
alias nr='npm run' # Run npm script
alias ng='sudo npm i -g' # Install npm script globall
alias ns='npm i -S' # Install npm package to dependencies
alias nd='npm i -D' # Install npm package to devDependencies
alias npmE='PATH="$(npm bin)":"$PATH"' # Run npm package based on directory

# Pacman
alias pacup='sudo pacman -Syu' # update all packages
alias pacen='sudo pacman -Sc' # clean cache, remove unsued repos
alias yup='yaourt -Syu --aur' # update all packages including aur
alias yip='yaourt -Syu' # update all packages
alias yen='yaourt -Sc' # clean cache, including aur
alias mirror='sudo cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.old && sudo reflector --verbose --country "United States" -l 200 -p http --sort rate --save /etc/pacman.d/mirrorlist ' # update to fastest mirrors

# Pip
alias pipup='sudo pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs pip install -U'

# Systemd
alias sstatus='sudo systemctl status' # get status of process
alias sstart='sudo systemctl start' # state a process
alias sstop='sudo systemctl stop' # stop a process
alias sen='sudo systemctl enable' # start a process at boot
alias sdi='sudo systemctl disable' # stop a proess from starting at boot

# ?
alias fuck='eval $(thefuck $(fc -ln -1 | tail -n 1)); fc -R'
alias cls='clear'
