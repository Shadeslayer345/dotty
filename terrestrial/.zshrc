# ----------------------------------------------------
# ZSH configuration
# ----------------------------------------------------


# ----------------------------------------------------
# OH MY ZSH
#   - path
#   - theme
#   - plugins
# ----------------------------------------------------

export ZSH=$HOME/.oh-my-zsh

ZSH_THEME="norm"

COMPLETION_WAITING_DOTS="true"

plugins=(archlinux chucknorris sudo zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# ---------------------------------------------------
# User configuration
#   - Environment
#       - Path
#       - XDG (for various applications)
#       - Language
#       - Editor
#       - Compilation
# ---------------------------------------------------

RUBY_ENV="/home/japy/.gem/ruby/2.2.0/bin"
PYTHONSTARTUP=$HOME/.config/python/.startup

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:"$RUBY_ENV

export XDG_CONFIG_HOME=$HOME/.config

export LANG=en_US.UTF-8

export EDITOR='vim'

export ARCHFLAGS="-arch x86_64"

export PYTHONSTARTUP

# ---------------------------------------------------
# Z - cli navigator
# ---------------------------------------------------

. ~/.oh-my-zsh/custom/plugins/z/z.sh

# ----------------------------------------------------
# Other
#   - Highlighting
#   - Colors
#   - Aliases
#   - Functions
# ----------------------------------------------------

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)
source ~/.config/zsh/colors.zsh
source ~/.config/zsh/aliases.zsh
source ~/.config/zsh/functions.zsh

