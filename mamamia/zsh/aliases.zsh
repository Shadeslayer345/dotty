# ----------------------------------------------------
# Aliases
#   - MIPS
#   - Network
#   - NPM
#   - Pacman
#   - Pip
#   - Systemd
# ----------------------------------------------------

# MIPS
alias spimf='spim -file' # compile MIPS files

# Network
alias csunix='ssh hbarry@138.238.148.14' # connect SYCS class server
alias nyan='nc -v nyancat.dakko.us 23' # Nyan Cat!
alias sw='telnet towel.blinkenlights.nl' # Star Wars Episode IV, in ascii

# NPM
alias npmr='npm run'
alias ng='npm i -g'
alias ns='npm i -S'
alias nd='npm i -D'

# Pacman
alias pacup='sudo apt-get update && sudo apt-get dist-upgrade' # update all packages
alias pacin='sudo apt-get install' # install package
alias pacen='sudo apt-get clean && sudo apt-get autoclean' # clean cache, remove unsued repos
alias pacrem='sudo apt-get --purge remove' # remove package
alias pacreps='sudo apt-cache search' # search for pacakes
# Pip
alias pipup='sudo pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs pip install -U'

# Systemd
alias sstatus='sudo systemctl status' # get status of process
alias sstart='sudo systemctl start' # state a process
alias sstop='sudo systemctl stop' # stop a process
alias sen='sudo systemctl enable' # start a process at boot
alias sdi='sudo systemctl disable' # stop a proess from starting at boot

# ?
alias fuck='eval $(thefuck $(fc -ln -1 | tail -n 1)); fc -R'
alias cls='clear'
